class Commentators {
    constructor(io) {
        this.io = io
    }


    generateMessage(type, options) {
        const message = this.messageFactory(type, options);
        this.io.to('racing').to('after-race').emit('commentators-message', message);
    }



    // Factory Pattern
    ////////////////////////////////////////////////////////////////////////////////////////////////
    messageFactory(messageType, options = {}) {
        switch (messageType) {
            case 'startRace':
                const raceStartMessage = new RaceStartMessage(options.racers);
                return {
                    message: raceStartMessage.getMessage()
                };
            case 'raceProgress':
                const raceProgressMessage = new RaceProgressMessage(options.racers);
                return {
                    message: raceProgressMessage.getMessage()
                };

            case 'preFinish':
                const finishLineMessage = new FinishLineMessage(options.racerName);
                return {
                    message: finishLineMessage.getMessage()
                };
            case 'finish':
                const finishMessage = new FinishMessage(options.racerName);
                return {
                    message: finishMessage.getMessage()
                };
            case 'endRace':
                const raceEndMessage = new RaceEndMessage(options.racers);
                return {
                    message: raceEndMessage.getMessage()
                };
            default:
                const commentatorMessage = new CommentatorMessage();
                return commentatorMessage.getMessage();
        }

    }

}

class CommentatorsMessage {
    constructor() {}

    getMessage() {
        return 'hi'
    }
}

class FinishLineMessage extends CommentatorsMessage {
    constructor(racerName) {
        super()
        this.racerName = racerName;
    }

    getMessage() {
        return `${this.racerName } on the finish line! 30 taps left`
    }

}

class FinishMessage extends CommentatorsMessage {
    constructor(racerName) {
        super();
        this.racerName = racerName
    }

    getMessage() {
        return `${this.racerName} finished the race`
    }
}

class InfoMessage extends CommentatorsMessage {
    constructor(racers) {
        super();
        this.racers = racers;
    }

    getMessage() {}

}

class RaceProgressMessage extends InfoMessage {
    constructor(racers) {
        super(racers)
    }

    getMessageText() {
        return 'sdwefwefwe'
    }
}

class RaceEndMessage extends InfoMessage {
    constructor(racers) {
        super(racers)
    }

    getMessage() {
        const prizePlaces = this.racers.slice(0, 3).reduce((messageString, {
            username,
            usedTime,
            leaved
        }, index) => {
            let result;
            if (leaved) {
                result = `${username} left the race.`
            } else {
                result = `${username} finish on  ${index + 1} position with the record of ${usedTime}. `;
            }
            return `${messageString}${result}`;
        });

        return `${prizePlaces} Thanks for attention!`

    }
}

class RaceStartMessage extends InfoMessage {

    constructor(racers) {
        super(racers);
    }

    getMessage() {
        return this.racers.reduce((messageString, racer) => {
            return `${messageString} - ${racer.username} `;
        }, 'List of racers:');
    }

}


module.exports = {
    Commentators
};