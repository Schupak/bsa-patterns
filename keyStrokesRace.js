const jwt = require('jsonwebtoken');
const texts = require('./data/texts.json');
const {
    Commentators
} = require('./models/Commentators');


// Facade Pattern
/////////////////////////////////////////////////////////////////////
class KeyStrokesRace {
    constructor({
        io
    }) {
        this.io = io;
        this.raceTime = 100;
        this.waitTime = 10;
        this.waitCountdown = this.raceTime + this.waitTime;
        this.waitingCountdown = this.waitCountdown;
        this.timer = null;
        this.raceStarted = false;
        this.currentText = this.getRaceText();
        this.commentators = new Commentators(io);

    }


    startRace() {
        this.currentText = this.getRaceText();
        this.initRacers();
        this.io.to('racing').emit('startRace', {
            racers: this.racers
        });
        this.commentators.generateMessage('startRace', {
            racers: this.racers
        })
        this.toggleRaceState();
    }

    stopRace() {
        this.toggleRaceState();
        const afterRaceClients = this.io.sockets.adapter.rooms['after-race'].sockets;
        Object.keys(afterRaceClients).forEach(socketId => {
            const socket = this.io.sockets.connected[socketId];
            socket.leave('after-race');
            socket.join('waiting');
        });

        this.commentators.generateMessage('raceEnd', {
            racers: this.racers
        });

    }

    startPlaying() {
        this.waitingCountdown = 10;
        this.io.to('waiting').emit('waiting-countdown', {
            time: this.waitingCountdown
        });

        this.timer = setInterval(() => {
            this.waitingCountdown--;
            if (this.waitingCountdown === 0) {
                this.startRace();
                this.resetCountdown();
            }
            this.racingCountdown = this.waitingCountdown - this.waitTime;
            if (this.racingCountdown === 0 && this.raceStarted) {
                this.stopRace();
            }

            this.io.to('waiting').to('after-race').emit('waiting-countdown', {
                time: this.waitingCountdown
            });
            this.io.to('racing').emit('racing-countdown', {
                time: this.racingCountdown
            });

        }, 1000);
    };

    stopPlaying() {
        clearInterval(this.timer);
        this.resetCountdown();
        this.raceStarted = false;
    };

    initRacers() {
        const waitingClients = this.io.sockets.adapter.rooms['waiting'].sockets;
        this.racers = Object.keys(waitingClients).map(socketId => {
            const socket = this.io.sockets.connected[socketId];
            const {
                username
            } = this.getUserFromJWT(socket.handshake.query.token);
            socket.leave('waiting');
            socket.join('racing');

            socket.on('char-entered', ({
                token
            }) => {
                jwt.verify(token, 'secret', (error, {
                    username
                }) => {
                    if (!error) {
                        const userIndex = this.racers.findIndex(user => user.username === username);
                        this.racers[userIndex].enteredSymbols++;
                        if (this.currentText.length - this.racers[userIndex].enteredSymbols === 30) {
                            this.commentators.generateMessage('preFinish', {
                                racerName: username
                            });
                        }
                        this.io.to('racing').to('after-race').emit('racers-data', {
                            racers: this.racers
                        });
                    } else {
                        console.log(error);
                    }
                });
            });

            socket.on('finished', ({
                token
            }) => {
                jwt.verify(token, 'secret', (error, {
                    username
                }) => {
                    if (!error) {
                        const userIndex = this.racers.findIndex(user => user.username === username);
                        this.racers[userIndex].usedTime = this.racingCountdown;

                        this.io.to('racing').to('after-race').emit('racers-data', {
                            racers: this.racers
                        });
                        socket.leave('racing');
                        socket.join('after-race');
                        socket.emit('end-race');
                        this.commentators.generateMessage('finish', {
                            racerName: username
                        });

                    } else {
                        console.log(error);
                    }
                });
            });

            socket.on('disconnect', () => {
                if (!this.io.engine.clientsCount) {
                    this.racers = null;
                } else {
                    const userIndex = this.racers.findIndex(user => user.username === username);
                    this.racers[userIndex].leaved = true;
                    this.io.to('racing').emit('racers-data', {
                        racers: this.racers
                    });
                }
            });

            return {
                username,
                usedTime: 0,
                enteredSymbols: 0,
                leaved: false
            };
        });
    }

    getRaceText() {
        return texts[Math.floor(Math.random() * texts.length)];
    }

    toggleRaceState() {
        this.raceStarted = !this.raceStarted;
    }

    resetCountdown() {
        this.waitingCountdown = this.waitCountdown;
    };

    getUserFromJWT(token) {
        const {
            username,
            email
        } = jwt.decode(token);
        return {
            username,
            email
        };
    }
}

module.exports = {
    KeyStrokesRace
};